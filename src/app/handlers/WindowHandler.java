package src.app.handlers;

import src.app.HardwareStore;
import src.services.CoreService;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class WindowHandler extends WindowAdapter {
    HardwareStore hardwareStore;
    CoreService coreService;

    /**
     * *******************************************************
     * Constructor
     *
     * @param coreService   the Core Service
     * @param hardwareStore the main frame
     ********************************************************/
    public WindowHandler(HardwareStore hardwareStore, CoreService coreService) {
        this.coreService = coreService;
        this.hardwareStore = hardwareStore;
    }

    /**
     * Window Closing Event
     *
     * @param event properties
     */
    public void windowClosing(WindowEvent event) {
        coreService.cleanup();
    }
}