package src.app.handlers;

import src.app.HardwareStore;
import src.app.views.records.UpdateRecordDialog;
import src.services.DataService;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * *******************************************************
 * Class:
 ********************************************************/
public class MouseClickedHandler extends MouseAdapter {
    JTable table;
    DataService dataService;

    /**
     * mouse click handler
     *
     * @param dataService the data service
     * @param tablePassed the table to manipulate
     */
    public MouseClickedHandler(DataService dataService, JTable tablePassed) {
        table = tablePassed;
        this.dataService = dataService;

    }

    /**
     * Mouse Click Event
     *
     * @param hws   the main fraim
     * @param event properties
     */
    public void mouseClicked(HardwareStore hws, MouseEvent event) {
        if (event.getSource() == table) {
            int rowIndex = table.getSelectedRow();
            JOptionPane.showMessageDialog(null,
                    "Enter the record ID to be updated and press enter.",
                    "Update src.entities.Record", JOptionPane.INFORMATION_MESSAGE);
            UpdateRecordDialog update = new UpdateRecordDialog(hws, dataService, rowIndex);
            if (rowIndex < 250) {
                update.setVisible(true);
                table.repaint();
            }
        }
    }
}
