package src.app.handlers;

import src.app.HardwareStore;
import src.app.views.password.PasswordDialog;
import src.app.views.records.DeleteRecordDialog;
import src.app.views.records.NewRecordDialog;
import src.app.views.records.UpdateRecordDialog;
import src.services.CoreService;
import src.services.DataService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class MenuHandler implements ActionListener {
    private final DataService dataService;
    private final CoreService coreService;
    private final HardwareStore hardwareStore;
    public JMenuBar menuBar;
    public JMenu fileMenu, viewMenu, optionsMenu, toolsMenu,
            helpMenu, aboutMenu;
    public JMenuItem exitMI;
    public JMenuItem lawnMowerMI, lawnMowingTractorMI, handDrillMI, drillPressPowerMI,
            hammersMI, circularSawsMI, tableMI, bandSawsMI, sandersMI,
            staplersMI, wetDryVacsMI, storageStatesCabinetMI;
    public JMenuItem deleteMI, addMI, updateMI, listAllMI;
    public JMenuItem debugON, debugOFF;
    public JMenuItem helpHWMI;
    public JMenuItem aboutHWMI;
    public PasswordDialog passwordDialog;
    public UpdateRecordDialog recordDialog;
    public NewRecordDialog newRecordDialog;
    public DeleteRecordDialog deleteRecordDialog;

    public MenuHandler(HardwareStore hardwareStore, CoreService coreService, DataService dataService) {
        this.dataService = dataService;
        this.coreService = coreService;
        this.hardwareStore = hardwareStore;
    }


    public void setupMenu() {
        createMenuBar();

        viewMenu = new JMenu("View");
        populateViewMenu();
        menuBar.add(viewMenu);

        optionsMenu = new JMenu("Options");
        populateOptionsMenu();
        menuBar.add(optionsMenu);

        toolsMenu = new JMenu("Tools");
        populateToolsMenu();
        menuBar.add(toolsMenu);

        helpMenu = new JMenu("Help");
        populateHelpMenu();
        menuBar.add(helpMenu);

        aboutMenu = new JMenu("About");
        populateAboutMenu();
        menuBar.add(aboutMenu);
    }

    private void populateAboutMenu() {
        aboutHWMI = new JMenuItem("About HW Store");
        aboutMenu.add(aboutHWMI);
        aboutHWMI.addActionListener(this);
    }

    private void populateHelpMenu() {
        helpHWMI = new JMenuItem("Help on HW Store");
        helpMenu.add(helpHWMI);
        helpHWMI.addActionListener(this);
    }

    private void populateToolsMenu() {
        debugON = new JMenuItem("Debug On");
        debugOFF = new JMenuItem("Debug Off");
        toolsMenu.add(debugON);
        toolsMenu.add(debugOFF);
        debugON.addActionListener(this);
        debugOFF.addActionListener(this);
    }

    private void populateOptionsMenu() {
        listAllMI = new JMenuItem("List All");
        optionsMenu.add(listAllMI);
        listAllMI.addActionListener(this);
        optionsMenu.addSeparator();

        addMI = new JMenuItem("Add");
        optionsMenu.add(addMI);
        addMI.addActionListener(this);

        updateMI = new JMenuItem("Update");
        optionsMenu.add(updateMI);
        updateMI.addActionListener(this);
        optionsMenu.addSeparator();

        deleteMI = new JMenuItem("Delete");
        optionsMenu.add(deleteMI);
        deleteMI.addActionListener(this);
    }

    private void populateViewMenu() {
        lawnMowerMI = new JMenuItem("Lawn Mowers");
        viewMenu.add(lawnMowerMI);
        lawnMowerMI.addActionListener(this);

        lawnMowingTractorMI = new JMenuItem("Lawn Mowing Tractors");
        viewMenu.add(lawnMowingTractorMI);
        lawnMowingTractorMI.addActionListener(this);

        handDrillMI = new JMenuItem("Hand Drills Tools");
        viewMenu.add(handDrillMI);
        handDrillMI.addActionListener(this);

        drillPressPowerMI = new JMenuItem("Drill Press Power Tools");
        viewMenu.add(drillPressPowerMI);
        drillPressPowerMI.addActionListener(this);

        circularSawsMI = new JMenuItem("Circular Saws");
        viewMenu.add(circularSawsMI);
        circularSawsMI.addActionListener(this);

        hammersMI = new JMenuItem("Hammers");
        viewMenu.add(hammersMI);
        hammersMI.addActionListener(this);

        tableMI = new JMenuItem("Table Saws");
        viewMenu.add(tableMI);
        tableMI.addActionListener(this);

        bandSawsMI = new JMenuItem("Band Saws");
        viewMenu.add(bandSawsMI);
        bandSawsMI.addActionListener(this);

        sandersMI = new JMenuItem("Sanders");
        viewMenu.add(sandersMI);
        sandersMI.addActionListener(this);


        staplersMI = new JMenuItem("Staplers");
        viewMenu.add(staplersMI);
        staplersMI.addActionListener(this);

        wetDryVacsMI = new JMenuItem("Wet-Dry Vacs");
        viewMenu.add(wetDryVacsMI);
        wetDryVacsMI.addActionListener(this);

        storageStatesCabinetMI = new JMenuItem("Storage, Chests & Cabinets");
        viewMenu.add(storageStatesCabinetMI);
        storageStatesCabinetMI.addActionListener(this);
    }

    /**
     * Add the menubar to the frame
     */
    private void createMenuBar() {
        menuBar = new JMenuBar();

        hardwareStore.setJMenuBar(menuBar);

        fileMenu = new JMenu("File");

        menuBar.add(fileMenu);
        /** Add the Exit menuitems */
        exitMI = new JMenuItem("Exit");
        fileMenu.add(exitMI);
        exitMI.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == exitMI) {
            /**The Exit menu Item was selected. */
            coreService.cleanup();
        } else if (e.getSource() == lawnMowerMI) {
            coreService.sysPrint("The Lawn Mower menu Item was selected.\n");

            coreService.display("Lawn Mowers");
        } else if (e.getSource() == lawnMowingTractorMI) {
            coreService.sysPrint("The Lawn Mower Tractor menu Item was selected.\n");

            coreService.display("Lawn Tractor Mowers");
        } else if (e.getSource() == handDrillMI) {
            coreService.sysPrint("The Hand Drill Tools menu Item was selected.\n");

            coreService.display("Hand Drill Tools");
        } else if (e.getSource() == drillPressPowerMI) {
            coreService.sysPrint("The Drill Press Power Tools menu Item was selected.\n");

            coreService.display("Drill Press Power Tools");
        } else if (e.getSource() == circularSawsMI) {
            coreService.sysPrint("The Circular Saws Tools menu Item was selected.\n");

            coreService.display("Circular Saws");
        } else if (e.getSource() == hammersMI) {
            coreService.sysPrint("The Hammer menu Item was selected.\n");

            coreService.display("Hammers");
        } else if (e.getSource() == tableMI) {
            coreService.sysPrint("The Table Saws menu Item was selected.\n");

            coreService.display("Table Saws");
        } else if (e.getSource() == bandSawsMI) {
            coreService.sysPrint("The Band Saws menu Item was selected.\n");

            coreService.display("Band Saws");
        } else if (e.getSource() == sandersMI) {
            coreService.sysPrint("The Sanders menu Item was selected.\n");

            coreService.display("Sanders");
        } else if (e.getSource() == staplersMI) {
            coreService.sysPrint("The Staplers menu Item was selected.\n");

            coreService.display("Staplers");
        } else if (e.getSource() == wetDryVacsMI) {
            coreService.sysPrint("The Wet-Dry Vacs menu Item was selected.\n");
            // ListRecs BPTRecs = new ListRecs( hws , "WDV", "Wet-Dry Vacs" );
        } else if (e.getSource() == storageStatesCabinetMI) {
            coreService.sysPrint("The Storage, Chests & Cabinets menu Item was selected.\n");
            //ListRecs BPTRecs = new ListRecs( hws , "SCC", "Storage, Chests & Cabinets" );
        } else if (e.getSource() == deleteMI) {
            coreService.sysPrint("The Delete src.entities.Record Dialog was made visible.\n");
            ;
            //DeleteRec( src.app.HardwareStore hw_store,  RandomAccessFile f,
            // JTable tab, String p_Data[] []  )
            deleteRecordDialog = new DeleteRecordDialog(hardwareStore, dataService);
            deleteRecordDialog.setVisible(true);
        } else if (e.getSource() == addMI) {
            coreService.sysPrint("The Add menu Item was selected.\n");
            passwordDialog.displayDialog("add");
        } else if (e.getSource() == updateMI) {
            coreService.sysPrint("The Update menu Item was selected.\n");
            recordDialog = new UpdateRecordDialog(hardwareStore, dataService, -1);
            recordDialog.setVisible(true);
        } else if (e.getSource() == listAllMI) {
            coreService.sysPrint("The List All menu Item was selected.\n");
            //listRecs.setVisible( true );
        } else if (e.getSource() == debugON) {
            coreService.setDebugMode(true);
            coreService.sysPrint("Debugging for this execution is turned on.\n");
        } else if (e.getSource() == debugOFF) {
            coreService.sysPrint("Debugging for this execution is turned off.\n");
            coreService.setDebugMode(false);
        } else if (e.getSource() == helpHWMI) {
            coreService.sysPrint("The Help menu Item was selected.\n");
            File hd = new File("HW_Tutorial.html");
            //File net = new File("Netscp.exe");
            //System.out.println( "the path for help_doc is " + hd.getAbsolutePath() );
            //System.out.println( "the path for netscape is " + net.getAbsolutePath() );

            Runtime rt = Runtime.getRuntime();
            //String[] callAndArgs = { "d:\\Program Files\\netscape\\netscape\\Netscp.exe" ,
            String[] callAndArgs = {"c:\\Program Files\\Internet Explorer\\IEXPLORE.exe",
                    "" + hd.getAbsolutePath()};

            try {

                Process child = rt.exec(callAndArgs);
                child.waitFor();
                coreService.sysPrint("Process exit code is: " +
                        child.exitValue());
            } catch (IOException e2) {
                coreService.sysPrint(
                        "IOException starting process!");
            } catch (InterruptedException e3) {
                System.err.println(
                        "Interrupted waiting for process!");
            }
        } else if (e.getSource() == aboutHWMI) {
            coreService.sysPrint("The About menu Item was selected.\n");
            Runtime rt = Runtime.getRuntime();
            String[] callAndArgs = {"c:\\Program Files\\Internet Explorer\\IEXPLORE.exe",
                    "http://www.sumtotalz.com/TotalAppsWorks/ProgrammingResource.html"};
            try {
                Process child = rt.exec(callAndArgs);
                child.waitFor();
                coreService.sysPrint("Process exit code is: " +
                        child.exitValue());
            } catch (IOException e2) {
                System.err.println(
                        "IOException starting process!");
            } catch (InterruptedException e3) {
                System.err.println(
                        "Interrupted waiting for process!");
            }
        }
        String current = (String) e.getActionCommand();
    }
}
