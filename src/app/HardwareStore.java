/**
 * **************************************************
 * File:  src.app.HardwareStore.java
 * <p>
 * This program reads a random access file sequentially,
 * updates data already written to the file, creates new
 * data to be placed in the file, and deletes data
 * already in the file.
 * <p>
 * <p>
 * Copyright (c) 2002-2003 Advanced Applications Total Applications Works.
 * (AATAW)  All Rights Reserved.
 * <p>
 * AATAW grants you ("Licensee") a non-exclusive, royalty free, license to use,
 * modify and redistribute this software in source and binary code form,
 * provided that i) this copyright notice and license appear on all copies of
 * the software; and ii) Licensee does not utilize the software in a manner
 * which is disparaging to AATAW.
 * <p>
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. AATAW AND ITS LICENSORS SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING
 * OR DISTRIBUTING THE SOFTWARE OR ITS DERIVATIVES. IN NO EVENT WILL AATAW OR ITS
 * LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT,
 * INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF THE USE OF
 * OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 * <p>
 * This software is not designed or intended for use in on-line control of
 * aircraft, air traffic, aircraft navigation or aircraft communications; or in
 * the design, construction, operation or maintenance of any nuclear
 * facility. Licensee represents and warrants that it will not use or
 * redistribute the Software for such purposes.
 */

package src.app;


import src.app.handlers.MenuHandler;
import src.app.handlers.MouseClickedHandler;
import src.app.handlers.WindowHandler;
import src.app.views.password.PasswordDialog;
import src.app.views.records.DeleteRecordDialog;
import src.app.views.records.NewRecordDialog;
import src.app.views.records.UpdateRecordDialog;
import src.repositories.Datas;
import src.services.CoreService;
import src.services.DataService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.RandomAccessFile;


/**
 *
 * <p>Title: src.app.HardwareStore </p>
 * <p>Description: The src.app.HardwareStore application is a program that is used to display
 * hardware items and it allows these items to be created, updated, and/or
 * deleted.</p>
 * <p>Copyright: Copyright (c)</p>
 * <p>Company: TAW</p>
 * @author unascribed
 * @version 2.0
 */
public class HardwareStore extends JFrame
        implements ActionListener {
   private final Container container;
   private final DataService dataService = new DataService();
   private final CoreService coreService = new CoreService(this, dataService);
   private final MenuHandler menuHandler = new MenuHandler(this, coreService, dataService);
   public JTable table;
   private JButton cancel, refresh;

   /**
    * HardwareStore constructor : initialize the GUI
    */
   public HardwareStore() {
      super("Hardware Store: Lawn Mower ");

      dataService.setCoreService(coreService);
      dataService.aFile = new File("lawnmower.dat");

      container = getContentPane();

      menuHandler.setupMenu();
      initRecords();
      dataService.setup();
      setup();

      addWindowListener(new WindowHandler(this, coreService));
      setSize(700, 400);
      setVisible(true);
   }

   /**
    * entry point
    */
   public static void main(String args[]) {
      new HardwareStore();
   }

   /**
    * Initialize the components
    */
   public void setup() {
      double loopLimit = 0.0;

      dataService.setup();

      //Initialize main Table
      table = new JTable(dataService.entries, Datas.columnNames);
      table.addMouseListener(new MouseClickedHandler(dataService, table));
      table.setEnabled(true);

      // Add the main component into the frame
      container.add(table, BorderLayout.CENTER);
      container.add(new JScrollPane(table));
      cancel = new JButton("Cancel");
      refresh = new JButton("Refresh");
      JPanel buttonPanel = new JPanel();
      buttonPanel.add(cancel);
      container.add(buttonPanel, BorderLayout.SOUTH);

      refresh.addActionListener(this);
      cancel.addActionListener(this);

      menuHandler.recordDialog = new UpdateRecordDialog(this, dataService, -1);
      menuHandler.deleteRecordDialog = new DeleteRecordDialog(this, dataService);
      /** Allocate pWord last; otherwise  the update,
       *  newRec and deleteRec references will be null
       *  when the PassWrod class attempts to use them.*/
      menuHandler.passwordDialog = new PasswordDialog(this);
   }

   /**
    * Initialize the datas
    */
   private void initRecords() {
      dataService.InitRecord("lawnmower.dat", Datas.lawnMower, 27);
      dataService.InitRecord("lawnTractor.dat", Datas.lawnTractor, 11);
      dataService.InitRecord("handDrill.dat", Datas.handDrill, 15);
      dataService.InitRecord("drillPress.dat", Datas.drillPress, 10);
      dataService.InitRecord("circularSaw.dat", Datas.circularSaw, 12);
      dataService.InitRecord("hammer.dat", Datas.hammer, 12);
      dataService.InitRecord("tableSaw.dat", Datas.tableSaw, 15);
      dataService.InitRecord("bandSaw.dat", Datas.bandSaw, 10);
      dataService.InitRecord("sanders.dat", Datas.sanders, 15);
      dataService.InitRecord("stapler.dat", Datas.stapler, 15);
   }

   /**
    * Repopulate/refresh the table
    * @param file : file with concerned entries
    * @param entries : entries to display
    */
   public void redisplay(RandomAccessFile file, String entries[][]) {
      for (int index = 0; index < dataService.numEntries + 5; index++) {
         entries[index][0] = "";
         entries[index][1] = "";
         entries[index][2] = "";
         entries[index][3] = "";
         entries[index][4] = "";
         entries[index][5] = "";
         entries[index][6] = "";
      }
      int numEntries = coreService.toArray(file, entries);
      coreService.sysPrint("Redisplay(): 1  - The number of entries is " + numEntries);
      dataService.setNumEntries(numEntries);
      ;
      container.remove(table);
      table = new JTable(entries, Datas.columnNames);
      table.setEnabled(true);
      container.add(table, BorderLayout.CENTER);
      container.add(new JScrollPane(table));
      container.validate();
   }

   /**
    *  cancel button's handler
    */
   public void actionPerformed(ActionEvent e) {

      if (e.getSource() == refresh) {
         coreService.sysPrint("\nThe Refresh button was pressed. ");
         Container cc = getContentPane();

         table = new JTable(dataService.entries, Datas.columnNames);
         cc.validate();
      } else if (e.getSource() == cancel)
         coreService.cleanup();
   }

   public void displayDeleteDialog() {
      coreService.sysPrint("The Delete src.entities.Record Dialog was made visible.\n");
      menuHandler.deleteRecordDialog.setVisible(true);
   }

   public void displayUpdateDialog() {
      coreService.sysPrint("The Update src.entities.Record Dialog was made visible.\n");
      JOptionPane.showMessageDialog(null,
              "Enter the record ID to be updated and press enter.",
              "Update src.entities.Record", JOptionPane.INFORMATION_MESSAGE);
      menuHandler.recordDialog = new UpdateRecordDialog(this, dataService, -1);
      menuHandler.recordDialog.setVisible(true);
   }

   public void displayAddDialog() {
      coreService.sysPrint("The New/Add src.entities.Record Dialog was made visible.\n");
      menuHandler.newRecordDialog = new NewRecordDialog(this, dataService, coreService);
      menuHandler.newRecordDialog.setVisible(true);
   }
}

