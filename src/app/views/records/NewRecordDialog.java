package src.app.views.records;

import src.app.HardwareStore;
import src.entities.Record;
import src.services.CoreService;
import src.services.DataService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * *******************************************************
 * class NewRec is used to gather and insert data for new
 * hardware item records.
 ********************************************************/
public class NewRecordDialog extends Dialog implements ActionListener {

    private RandomAccessFile file;
    private JTextField recID, toolType, brandName, toolDesc,
            partNum, quantity, price;
    private JLabel recIDLabel, toolTypeLabel, brandNameLabel,
            toolDescLabel, partNumLabel, quantityLabel,
            priceLabel;
    private JButton cancel, save;
    private Record data;
    private int theRecID, toCont, fileLen;
    private String pData[][];
    private String columnNames[] = {"src.entities.Record ID", "Type of tool",
            "Brand Name", "Tool Description", "partNum",
            "Quantity", "Price"};
    private HardwareStore hwStore;
    private DataService dataService;
    private CoreService coreService;
    private boolean found = true;

    /**
     * Method: NewRecordDialog() constructor
     *
     * @param coreService the Core Service
     * @param dataService the Data Service
     * @param hw_store    the main frame
     */
    public NewRecordDialog(HardwareStore hw_store, DataService dataService, CoreService coreService) {
        super(new Frame(), "New src.entities.Record", true);

        hwStore = hw_store;

        this.coreService = coreService;
        this.dataService = dataService;

        setup();
    }

    /**
     * Method: setup() initialize the frame
     */
    public void setup() {
        setSize(400, 250);
        setLayout(new GridLayout(9, 2));

        recID = new JTextField(10);
        recID.setEnabled(false);
        try {
            file = new RandomAccessFile(dataService.aFile, "rw");
            file.seek(0);
            fileLen = (int) file.length() / data.getSize();
            recID.setText("" + fileLen);
        } catch (IOException ex) {
            partNum.setText("Error reading file");
        }
        toolType = new JTextField(10);
        brandName = new JTextField(10);
        toolDesc = new JTextField(10);
        partNum = new JTextField(10);
        quantity = new JTextField(10);
        price = new JTextField(10);
        recIDLabel = new JLabel("src.entities.Record ID");
        toolTypeLabel = new JLabel("Type of Tool");
        brandNameLabel = new JLabel("Brand Name");
        toolDescLabel = new JLabel("Tool Description");
        partNumLabel = new JLabel("Part Number");
        quantityLabel = new JLabel("Quantity");
        priceLabel = new JLabel("Price");
        save = new JButton("Save Changes");
        cancel = new JButton("Cancel");

        recID.addActionListener(this);
        save.addActionListener(this);
        cancel.addActionListener(this);

        add(recIDLabel);
        add(recID);
        add(toolTypeLabel);
        add(toolType);
        add(brandNameLabel);
        add(brandName);
        add(toolDescLabel);
        add(toolDesc);
        add(partNumLabel);
        add(partNum);
        add(quantityLabel);
        add(quantity);
        add(priceLabel);
        add(price);
        add(save);
        add(cancel);

        data = new Record();
        JOptionPane.showMessageDialog(null,
                "The recID field is currently set to the next record ID.\n" +
                        "Please just fill in the " +
                        "remaining fields.",
                "RecID To Be Entered",
                JOptionPane.INFORMATION_MESSAGE);

    }

    /**
     * actionPerformed() button click event
     */
    public void actionPerformed(ActionEvent e) {
        try {
            file = new RandomAccessFile(dataService.aFile, "rw");
            file.seek(0);
            fileLen = (int) file.length() / data.getSize();
            recID.setText("" + fileLen);
        } catch (IOException ex) {
            partNum.setText("Error reading file");
        }

        if (e.getSource() == recID) {
            recID.setEnabled(false);
        } else if (e.getSource() == save) {
            if (!recID.getText().equals("")) {
                try {
                    data.setId(Integer.parseInt(recID.getText()));
                    data.setToolType(toolType.getText().trim());
                    data.setBrandName(brandName.getText().trim());
                    data.setToolDesc(toolDesc.getText().trim());
                    data.setPartNumber(partNum.getText().trim());
                    data.setQuantity(Integer.parseInt(quantity.getText()));
                    data.setCost(price.getText().trim());
                    file.seek(0);
                    file.seek((data.getId()) * data.getSize());
                    data.write(file);

                    // Account for index starting at 0 and for the next slot
                    theRecID = dataService.getNumEntries();
                    coreService.sysPrint("NewRec 1: The numbers of entries is " + (theRecID - 1));

                    coreService.sysPrint("NewRec 2: A new record is being added at " +
                            theRecID);
                    pData[theRecID][0] = Integer.toString(data.getId());
                    pData[theRecID][1] = data.getToolType().trim();
                    pData[theRecID][2] = data.getBrandName().trim();
                    pData[theRecID][3] = data.getToolDesc().trim();
                    pData[theRecID][4] = data.getPartNumber().trim();
                    pData[theRecID][5] = Integer.toString(data.getQuantity());
                    pData[theRecID][6] = data.getCost().trim();
                    hwStore.table = new JTable(pData, columnNames);
                    hwStore.table.repaint();
                    dataService.setNumEntries(dataService.getNumEntries() + 1);
                } catch (IOException ex) {
                    partNum.setText("Error writing file");
                    return;
                }
            }

            toCont = JOptionPane.showConfirmDialog(null,
                    "Do you want to add another record? \nChoose one",
                    "Choose one",
                    JOptionPane.YES_NO_OPTION);

            if (toCont == JOptionPane.YES_OPTION) {
                recID.setText("");
                toolType.setText("");
                quantity.setText("");
                brandName.setText("");
                toolDesc.setText("");
                partNum.setText("");
                price.setText("");
            } else {
                close();
            }
        } else if (e.getSource() == cancel) {
            close();
        }
    }

    /**
     * close() the window
     */
    private void close() {
        partNum.setText("");
        toolType.setText("");
        quantity.setText("");
        price.setText("");
        setVisible(false);
    }
}
