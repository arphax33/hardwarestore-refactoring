package src.app.views.records;

import src.app.HardwareStore;
import src.entities.Record;
import src.services.DataService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * **************************************************************
 * Class: DeleteRec is used to create the Delete src.entities.Record dialog,
 * which in turn, is used to to delete records from the specified
 * table(s).
 ******************************************************************/
public class DeleteRecordDialog extends Dialog
        implements ActionListener {
    private JTextField recID;
    private JLabel recIDLabel;
    private JButton cancel, delete;
    private Record data;
    private int partNum;
    private int theRecID = -1, toCont;
    private HardwareStore hwStore;
    private DataService dataService;
    private boolean found = false;

    /**
     * Method: DeleteRecordDialog() constructor
     *
     * @param dataService the Data Service
     * @param hw_store    the main frame
     */
    public DeleteRecordDialog(HardwareStore hw_store, DataService dataService) {

        super(new Frame(), "Delete src.entities.Record", true);
        setSize(400, 150);
        setLayout(new GridLayout(2, 2));
        hwStore = hw_store;
        setup();

        this.dataService = dataService;
    }

    /**
     * Method: setup() initialize the frame
     */
    public void setup() {
        recIDLabel = new JLabel("src.entities.Record ID");
        recID = new JTextField(10);
        delete = new JButton("Delete src.entities.Record");
        cancel = new JButton("Cancel");

        cancel.addActionListener(this);
        delete.addActionListener(this);
        recID.addActionListener(this);

        add(recIDLabel);
        add(recID);
        add(delete);
        add(cancel);

        data = new Record();
    }

    /**
     * actionPerformed() button click event
     */
    public void actionPerformed(ActionEvent e) {
        System.out.println("DeleteRec(): 1a - In the actionPerformed() method. ");
        if (e.getSource() == recID) {
            theRecID = Integer.parseInt(recID.getText());

            if (theRecID < 0 || theRecID > 250) {
                recID.setText("Invalid part number");
                //return;
            } else {

                try {
                    dataService.file = new RandomAccessFile(dataService.aFile, "rw");

                    dataService.file.seek(theRecID * data.getSize());
                    data.ReadRec(dataService.file);
                    System.out.println("DeleteRec(): 1b - The record read is recid " +
                            data.getId() + " " +
                            data.getToolType() + " " +
                            data.getBrandName() + " " +
                            data.getToolDesc() + " " +
                            data.getQuantity() + " " +
                            data.getCost());
                } catch (IOException ex) {
                    recID.setText("Error reading file");
                }
            }
        } else if (e.getSource() == delete) {
            theRecID = Integer.parseInt(recID.getText());

            for (int iii = 0; iii < dataService.entries.length; iii++) {
                if ((dataService.entries[iii][0]).equals("" + theRecID)) {
                    theRecID = Integer.parseInt(dataService.entries[iii][0]);
                    found = true;
                    System.out.println("DeleteRec(): 2 - The record id was found is  "
                            + dataService.entries[iii][0]);
                    break;
                }
            }

            try {

                System.out.println("DeleteRec(): 3 - The data file is " + dataService.aFile +
                        "The record to be deleted is " + theRecID);
                dataService.file = new RandomAccessFile(dataService.aFile, "rw");
                data.setId(theRecID);

                dataService.setNumEntries(dataService.getNumEntries() - 1);
                System.out.println("DeleteRec(): 4 - Go to the beginning of the file.");
                dataService.file.seek((0));
                dataService.file.seek((theRecID) * data.getSize());
                data.ReadRec(dataService.file);
                System.out.println("DeleteRec(): 5 - Go to the record " + theRecID + " to be deleted.");
                data.setId(-1);

                System.out.println("DeleteRec(): 6 - Write the deleted file to the file.");
                dataService.file.seek((0));
                dataService.file.seek((theRecID) * data.getSize());
                data.write(dataService.file, -1);

                System.out.println("DeleteRec(): 7 - The number of entries is " +
                        dataService.getNumEntries());

                dataService.file.close();
            } catch (IOException ex) {
                recID.setText("Error writing file");
                return;
            }


            toCont = JOptionPane.showConfirmDialog(null,
                    "Do you want to delete another record? \nChoose one",
                    "Select Yes or No",
                    JOptionPane.YES_NO_OPTION);

            if (toCont == JOptionPane.YES_OPTION) {
                recID.setText("");
            } else {
                close();
            }
        } else if (e.getSource() == cancel) {
            close();
        }
    }

    /**
     * close() the window
     */
    private void close() {
        try {
            System.out.println("DeleteRec(): 3 - The data file is " + dataService.aFile +
                    "The record to be deleted is " + theRecID);
            dataService.file = new RandomAccessFile(dataService.aFile, "rw");

            hwStore.redisplay(dataService.file, dataService.entries);
            ;
            dataService.file.close();
        } catch (IOException ex) {
            recID.setText("Error writing file");
            return;
        }
        setVisible(false);
        recID.setText("");
    }
}