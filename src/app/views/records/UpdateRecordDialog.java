package src.app.views.records;

import src.app.HardwareStore;
import src.entities.Record;
import src.services.DataService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * *******************************************************
 * class: UpdateRec
 ********************************************************/
public class UpdateRecordDialog extends Dialog implements ActionListener {
    private JTextField recID, toolType, brandName, toolDesc,
            partNum, quantity, price;
    private JLabel recIDLabel, toolTypeLabel, brandNameLabel,
            toolDescLabel, partNumLabel, quantityLabel,
            priceLabel;
    private JButton cancel, save;
    private Record data;
    private int theRecID, ii, iii, toCont, loopCtrl;
    private HardwareStore hwstore;
    private DataService dataService;
    private boolean found = false;

    /**
     * *******************************************************
     * Method: UpdateRec()
     ********************************************************/
    public UpdateRecordDialog(HardwareStore hw_store, DataService dataService, int iiPassed) {


        super(new Frame(), "Update src.entities.Record", true);
        setSize(400, 280);
        setLayout(new GridLayout(9, 2));
        ii = iiPassed;
        hwstore = hw_store;

        this.dataService = dataService;

        setup();
    }

    /**
     * Method: setup() initialize the frame
     */
    public void setup() {

        /** create the text fields */
        recID = new JTextField(10);
        toolType = new JTextField(10);
        brandName = new JTextField(10);
        toolDesc = new JTextField(10);
        partNum = new JTextField(10);
        quantity = new JTextField(10);
        price = new JTextField(10);

        /** create the labels */
        recIDLabel = new JLabel("src.entities.Record ID");
        toolTypeLabel = new JLabel("Type of Tool");
        brandNameLabel = new JLabel("Brand Name");
        toolDescLabel = new JLabel("Tool Description");
        partNumLabel = new JLabel("Part Number");
        quantityLabel = new JLabel("Quantity");
        priceLabel = new JLabel("Price");

        /** create the buttons */
        save = new JButton("Save Changes");
        cancel = new JButton("Cancel");

        /** attach the ActionListener */
        recID.addActionListener(this);
        save.addActionListener(this);
        cancel.addActionListener(this);

        /** Add the labels and text fields to the
         *  GridLayout manager context */
        add(recIDLabel);
        add(recID);
        add(toolTypeLabel);
        add(toolType);
        add(brandNameLabel);
        add(brandName);
        add(toolDescLabel);
        add(toolDesc);
        add(partNumLabel);
        add(partNum);
        add(quantityLabel);
        add(quantity);
        add(priceLabel);
        add(price);
        add(save);
        add(cancel);

        data = new Record();
    }

    /**
     * Method: isDigit() if converted string is a digit
     */
    public boolean isDigit(String strVal) {

        int strLength = 0;
        boolean isDigit = true;

        strLength = strVal.length();

        for (int ii = 0; ii < strLength; ii++) {
            if (!Character.isDigit(strVal.charAt(ii))) {
                isDigit = false;
                break;
            }
        }

        return isDigit;
    }

    /**
     * actionPerformed() button click event
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == recID) {
            if (isDigit(recID.getText())) {
                theRecID = Integer.parseInt(recID.getText());
            } else if (theRecID < 0 || theRecID > 250) {
                JOptionPane.showMessageDialog(null,
                        "A recID entered was:  less than 0 or greater than 250, which is invalid.\n" +
                                "Please enter a number greater than 0 and less than 251.", "RecID Entered",
                        JOptionPane.INFORMATION_MESSAGE);
                return;
            }

            theRecID = Integer.parseInt(recID.getText());

            System.out.println("UpdateRec(): 2a - The record id being sought is " + theRecID);

            for (int iii = 0; iii < dataService.entries.length; iii++) {
                if (dataService.entries[iii][0] != null) {
                    if (Integer.parseInt(dataService.entries[iii][0]) == theRecID) {
                        theRecID = Integer.parseInt(dataService.entries[iii][0]);
                        found = true;
                        System.out.println("UpdateRec(): 2b - The record id was found.");
                        break;
                    }
                }
            }

            try {

                dataService.file = new RandomAccessFile(dataService.aFile, "rw");
                dataService.file.seek((theRecID) * data.getSize());
                data.ReadRec(dataService.file);

                recID.setText("" + theRecID);
                toolType.setText(data.getToolType().trim());
                brandName.setText(data.getBrandName().trim());
                toolDesc.setText(data.getToolDesc().trim());
                partNum.setText(data.getPartNumber().trim());
                quantity.setText(Integer.toString(data.getQuantity()));
                price.setText(data.getCost().trim());
                System.out.println("UpdateRec(): 2c - The record found was " +
                        data.getId() + " " +
                        data.getBrandName() + " " +
                        data.getToolDesc() + " " +
                        data.getQuantity() + " " +
                        data.getCost() + " in file " + dataService.aFile);
            } catch (IOException ex) {
                recID.setText("UpdateRec(): 2d -  Error reading file");
            }

            if (data.getId() >= 0) {
               /*recID.setText( String.valueOf( data.getRecID() ) );
               toolType.setText( data.getToolType().trim() );
               brandName.setText( data.getBrandName().trim() ) ;
               toolDesc.setText( data.getToolDesc().trim() ) ;
               partNum.setText( data.getPartNumber().trim() ) ;
               quantity.setText( Integer.toString( data.getQuantity() ) );
               price.setText(  data.getCost().trim() ); */
            } else
                recID.setText("This record " +
                        theRecID + " does not exist");
        } else if (e.getSource() == save) {
            try {
                data.setId(Integer.parseInt(recID.getText()));
                data.setToolType(toolType.getText().trim());
                data.setBrandName(brandName.getText().trim());
                data.setToolDesc(toolDesc.getText().trim());
                data.setPartNumber(partNum.getText().trim());
                data.setQuantity(Integer.parseInt(quantity.getText().trim()));
                data.setCost(price.getText().trim());

                dataService.file.seek(0);
                dataService.file.seek(theRecID * data.getSize());
                data.write(dataService.file);

                System.out.println("UpdateRec(): 3 - The record found was " +
                        data.getId() + " " +
                        data.getBrandName() + " " +
                        data.getToolDesc() + " " +
                        data.getQuantity() + " " +
                        data.getCost() + " in file " + dataService.aFile);

                hwstore.redisplay(dataService.file, dataService.entries);
            } catch (IOException ex) {
                recID.setText("Error writing file");
                return;
            }

            toCont = JOptionPane.showConfirmDialog(null,
                    "Do you want to add another record? \nChoose one",
                    "Choose one",
                    JOptionPane.YES_NO_OPTION);

            if (toCont == JOptionPane.YES_OPTION) {
                recID.setText("");
                toolType.setText("");
                quantity.setText("");
                brandName.setText("");
                toolDesc.setText("");
                partNum.setText("");
                price.setText("");
            } else {
                close();
            }
        } else if (e.getSource() == cancel) {
            setVisible(false);
            close();
        }
    }

    /**
     * close() the window
     */
    private void close() {
        recID.setText("");
        brandName.setText("");
        quantity.setText("");
        price.setText("");
        setVisible(false);
    }
}

