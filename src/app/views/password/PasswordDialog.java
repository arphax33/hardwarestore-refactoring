package src.app.views.password;

import src.app.HardwareStore;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ************************************************************************
 *
 * <p> class PassWord </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c)</p>
 * <p>Company: TSSD</p>
 *
 * @author Ronald S. Holland
 * @version 1.0
 ****************************************************************************/
public class PasswordDialog extends Dialog
        implements ActionListener {

    private JButton cancel, enter;
    private JTextField userID;
    private JLabel userIDLabel, passwordLabel;
    private JPasswordField password;
    private JPanel buttonPanel, mainPanel;
    private Container c;
    private HardwareStore hwStore;
    private String whichDialog;

    /**
     * Method: PassWord() initialize frame components
     */
    public PasswordDialog(HardwareStore hw_Store) {
        super(new Frame(), "Password Check", true);

        hwStore = hw_Store;

        enter = new JButton("Enter");
        cancel = new JButton("Cancel");

        buttonPanel = new JPanel();
        mainPanel = new JPanel();

        /** declare the GridLayout manager for the mainPanel */
        mainPanel.setLayout(new GridLayout(3, 2));
        add(mainPanel, BorderLayout.CENTER);

        userID = new JTextField(10);
        password = new JPasswordField(10);

        userIDLabel = new JLabel("Enter your user ID");
        passwordLabel = new JLabel("Enter your user password");

        mainPanel.add(userIDLabel);
        mainPanel.add(userID);
        mainPanel.add(passwordLabel);
        mainPanel.add(password);

        buttonPanel.add(enter);
        buttonPanel.add(cancel);
        add(buttonPanel, BorderLayout.SOUTH);

        /** add the actionlisteners to the buttons */
        enter.addActionListener(this);
        cancel.addActionListener(this);

        setSize(400, 300);

    }

    /**
     * Method: displayDialog () display hardware CRUD management frame
     */
    public void displayDialog(String which_Dialog) {
        whichDialog = which_Dialog;

        /** set userid and password
         *  In a real application, the following two lines are not used.
         *  The dialog interrogates the user for an authorized userID and
         *  password. This information is preset in this case for convenience.*/
        userID.setText("admin");
        password.setText("hwstore");

        setVisible(true);
    }

    /**
     * Method: actionPerformed() on button click event
     *
     * @param event properties
     */
    public void actionPerformed(ActionEvent event) {

        if (event.getSource() == enter) {

            String pwd = new String(password.getPassword());
            String uID = new String(userID.getText());

            if ((uID.equals("admin")) &&
                    (pwd.equals("hwstore"))) {
                if (whichDialog == "delete") {
                    hwStore.displayDeleteDialog();
                    whichDialog = "closed";
                    userID.setText("");
                    password.setText("");
                    clear();
                } else if (whichDialog == "update") {
                    hwStore.displayUpdateDialog();
                    whichDialog = "closed";
                    userID.setText("");
                    password.setText("");
                    clear();
                } else if (whichDialog == "add") {
                    hwStore.displayAddDialog();
                    whichDialog = "closed";
                    userID.setText("");
                    password.setText("");
                    clear();
                }
            } else {
                JOptionPane.showMessageDialog(null,
                        "A userid or the password was incorrect.\n",
                        "Invalid Password", JOptionPane.INFORMATION_MESSAGE);
                userID.setText("");
                password.setText("");
            }
        }

        clear();
    }

    /**
     * Method: clear() close the dialog
     */
    private void clear() {
        setVisible(false);
        return;
    }

}
