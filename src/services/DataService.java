package src.services;

import src.entities.Record;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class DataService {
    public String entries[][] = new String[250][7];
    public RandomAccessFile file;
    public File aFile;
    public Record data;
    public int numEntries = 0, ZERO;
    private CoreService coreService;

    /**
     * Initialize datas
     */
    public void setup() {
        data = new Record();

        //determine the number of records in the file
        try {
            file = new RandomAccessFile("lawnmower.dat", "rw");

            aFile = new File("lawnmower.dat");

            numEntries = coreService.toArray(file, entries);

            file.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Create and initialize records into the table
     *
     * @param fileDat    : file name to initialize
     * @param entries    : initial datas
     * @param numEntries : is the number of entries in the array.
     */
    public void InitRecord(String fileDat, String entries[][],
                           int numEntries) {

        aFile = new File(fileDat);

        coreService.sysPrint("initRecord(): 1a - the value of fileData is " + aFile);

        try {
            /** Open the fileDat file in RW mode.
             *  If the file does not exist, create it
             *  and initialize it to 250 empty records.
             */

            coreService.sysPrint("initTire(): 1ab - checking to see if " + aFile + " exist.");
            if (!aFile.exists()) {

                coreService.sysPrint("initTire(): 1b - " + aFile + " does not exist.");

                file = new RandomAccessFile(aFile, "rw");
                data = new Record();

                for (int index = 0; index < numEntries; index++) {
                    data.setId(Integer.parseInt(entries[index][0]));
                    coreService.sysPrint("initTire(): 1c - The value of record ID is " + data.getId());
                    data.setToolType(entries[index][1]);
                    coreService.sysPrint("initTire(): 1cb - The length of ToolType is " + data.getToolType().length());
                    data.setBrandName(entries[index][2]);
                    data.setToolDesc(entries[index][3]);
                    coreService.sysPrint("initTire(): 1cc - The length of ToolDesc is " + data.getToolDesc().length());
                    data.setPartNumber(entries[index][4]);
                    data.setQuantity(Integer.parseInt(entries[index][5]));
                    data.setCost(entries[index][6]);

                    coreService.sysPrint("initTire(): 1d - Calling src.entities.Record method write() during initialization. " + index);
                    file.seek(index * Record.getSize());
                    data.write(file);

                }
            } else {
                coreService.sysPrint("initTire(): 1e - " + fileDat + " exists.");
                file = new RandomAccessFile(aFile, "rw");
            }

            file.close();
        } catch (IOException e) {
            System.err.println("InitRecord() " + e.toString() +
                    " " + aFile);
            System.exit(1);
        }
    }

    public String getPDataRow(int columnIndex, int rowIndex) {
        return entries[columnIndex][rowIndex];
    }

    public int getNumEntries() {
        return numEntries;
    }

    public void setNumEntries(int ent) {
        numEntries = ent;
    }

    public void setCoreService(CoreService coreService) {
        this.coreService = coreService;
    }
}
