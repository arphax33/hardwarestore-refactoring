package src.services;

import src.app.HardwareStore;
import src.entities.Record;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class CoreService {
    /**
     * This flag toggles debug
     */
    HardwareStore hardwareStore;
    DataService dataService;
    private boolean debugMode = false;

    public CoreService(HardwareStore hardwareStore, DataService dataService) {
        this.hardwareStore = hardwareStore;
        this.dataService = dataService;
    }

    /**
     * print to console only if debugMode is true
     *
     * @param str : message to print
     */
    public void sysPrint(String str) {
        if (debugMode) {
            System.out.println(str);
        }
    }

    /**
     * close file and exit application
     */
    public void cleanup() {
        try {
            dataService.file.close();
        } catch (IOException e) {
            System.exit(1);
        }

        hardwareStore.setVisible(false);
        System.exit(0);
    }

    /**
     * display content of the specified table
     *
     * @param str : table to display
     */
    public void display(String str) {

        String df = null, title = null;

        switch (str) {
            case "Lawn Mowers":
                df = new String("lawnmower.dat");
                dataService.aFile = new File("lawnmower.dat");
                title = new String("Hardware Store: Lawn Mowers");
                break;
            case "Lawn Tractor Mowers":
                df = new String("lawnTractor.dat");
                dataService.aFile = new File("lawnTractor.dat");
                title = new String("Hardware Store: Lawn Tractor Mowers");
                break;
            case "Hand Drill Tools":
                df = new String("handDrill.dat");
                dataService.aFile = new File("handDrill.dat");
                title = new String("Hardware Store:  Hand Drill Tools");
                break;
            case "Drill Press Power Tools":
                df = new String("drillPress.dat");
                dataService.aFile = new File("drillPress.dat");
                title = new String("Hardware Store: Drill Press Power Tools");
                break;
            case "Circular Saws":
                df = new String("circularSaw.dat");
                dataService.aFile = new File("circularSaw.dat");
                title = new String("Hardware Store: Circular Saws");
                break;
            case "Hammers":
                df = new String("hammer.dat");
                dataService.aFile = new File("hammer.dat");
                title = new String("Hardware Store: Hammers");
                break;
            case "Table Saws":
                df = new String("tableSaw.dat");
                dataService.aFile = new File("tableSaw.dat");
                title = new String("Hardware Store: Table Saws");
                break;
            case "Band Saws":
                df = new String("bandSaw.dat");
                dataService.aFile = new File("bandSaw.dat");
                title = new String("Hardware Store: Band Saws");
                break;
            case "Sanders":
                df = new String("sanders.dat");
                dataService.aFile = new File("sanders.dat");
                title = new String("Hardware Store: Sanders");
                break;
            case "Staplers":
                df = new String("stapler.dat");
                dataService.aFile = new File("stapler.dat");
                title = new String("Hardware Store: Staplers");
                break;
        }

        try {
            sysPrint("display(): 1a - checking to see if " + df + " exists.");
            if (!dataService.aFile.exists()) {

                sysPrint("display(): 1b - " + df + " does not exist.");

            } else {
                dataService.file = new RandomAccessFile(df, "rw");

                hardwareStore.setTitle(title);

                hardwareStore.redisplay(dataService.file, dataService.entries);
                ;
            }

            dataService.file.close();
        } catch (IOException e) {
            System.err.println(e.toString());
            System.err.println("Failed in opening " + df);
            System.exit(1);
        }

    }

    /**
     * Sort datas in the correct order
     *
     * @param file    : file to sort
     * @param entries to sort
     */
    public int toArray(RandomAccessFile file, String entries[][]) {

        Record NodeRef = new Record(),
                PreviousNode = null;
        int indexRow = 0, indexColumn = 0, fileSize = 0;

        try {
            fileSize = (int) file.length() / Record.getSize();
            sysPrint("toArray(): 1 - The size of the file is " + fileSize);
            if (fileSize > dataService.ZERO) {
                NodeRef.setFileLen(file.length());


                while (indexRow < fileSize) {
                    sysPrint("toArray(): 2 - NodeRef.getRecID is "
                            + NodeRef.getId());

                    file.seek(0);
                    file.seek(indexRow * NodeRef.getSize());
                    NodeRef.setFilePos(indexRow * NodeRef.getSize());
                    sysPrint("toArray(): 3 - input data file - Read record " + indexRow);
                    NodeRef.ReadRec(file);

                    String str2 = entries[indexRow][0];
                    sysPrint("toArray(): 4 - the value of entries[ indexRow ] [ 0 ] is " +
                            entries[0][0]);

                    if (NodeRef.getId() != -1) {
                        entries[indexColumn][0] = String.valueOf(NodeRef.getId());
                        entries[indexColumn][1] = NodeRef.getToolType().trim();
                        entries[indexColumn][2] = NodeRef.getBrandName().trim();
                        entries[indexColumn][3] = NodeRef.getToolDesc().trim();
                        entries[indexColumn][4] = NodeRef.getPartNumber().trim();
                        entries[indexColumn][5] = String.valueOf(NodeRef.getQuantity());
                        entries[indexColumn][6] = NodeRef.getCost().trim();

                        sysPrint("toArray(): 5 - 0- " + entries[indexColumn][0] +
                                " 1- " + entries[indexColumn][1] +
                                " 2- " + entries[indexColumn][2] +
                                " 3- " + entries[indexColumn][3] +
                                " 4- " + entries[indexColumn][4] +
                                " 5- " + entries[indexColumn][5] +
                                " 6- " + entries[indexColumn][6]);

                        indexColumn++;

                    } else {
                        sysPrint("toArray(): 5a the record ID is " + indexRow);
                    }

                    indexRow++;

                }  /** End of do-while loop   */
            }  /** End of outer if   */
        } catch (IOException ex) {
            sysPrint("toArray(): 6 - input data file failure. Index is " + indexRow
                    + "\nFilesize is " + fileSize);
        }

        return indexRow;

    }

    public void setDebugMode(boolean newDebugMode) {
        debugMode = newDebugMode;
    }
}
