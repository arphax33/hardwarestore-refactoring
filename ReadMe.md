<h1>A Basic Hardware Store</h1>

<h2>Educational content</h2>

<p>This repository is a copy of this [initial project]() for educational purposes. 
Some refactoring has been done to clarify the code</p>

<h2>Initial Description</h2>

<p>Rewritten on 11/30/05. In this rewrite, the GUI is virtually the same, 
but the underlying code has had a major rewrite. In the previous version, 
there was one table. In this version, we have gone to multiple tables, 
where a table represents one tool. In addition, we show how to initialize 
the table during the initial call to the src.app.HardwareStore constructor.
Updated 12/01/04 to loop on adds/updates/deletes. 
You can turn debug on/off from the menu bar. 
Send me an email if you have any problems with this example at ronholland@sumtotalz.com. 
</p>

1- RandomAccessFile processing
2- JMenu 
3- JTable
4- Event processing
5- etc.
This is written between the intermediate and advanced programmer levels. A tutorial and Src are included to show you how to build the example.

The author may have retained certain copyrights to this code...please observe their request and the law by reviewing all copyright conditions at the above URL.
